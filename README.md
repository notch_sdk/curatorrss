# CuratorRSS

This small Python Flask app turns a Curator.io stream into an RSS feed (that is tested with Notch's RSS Feed Node). The app acts as a mini webserver that Notch can call.

If you're testing and don't want to run this code yourself, Notch host a version of this app on the internet (with no guarantees for uptime or availability) with the following URLs:

https://curatorrss.notch.one/v1/posts/myCuratorApiKey/myCuratorFeedId

https://curatorrss.notch.one/v1/feeds/myCuratorApiKey

## What is Curator.io?

Curator.io is a social media aggregator and curation/censoring platform. It provides it's output via RESTful JSON based API - which Notch doesn't read natively. This example python app provides a small webserver that queries the Curator.io API and provides the data back as an RSS Feed.

You'll need a Curator.io Business account to gain access to the API.

## Getting the Feed IDs

You need the feed ID to be able to pull the feed. You can get a list of feed IDs via the example app. 

/v1/feeds/myCuratorApiKey

Where:

* myCuratorApiKey is your API key from the Curator.io applicaton


## Getting the RSS Feed

To retrieve a set of posts you need the feed ID and API key for the Curator.io feed you wish to present as RSS.

/v1/posts/myCuratorApiKey/myCuratorFeedID?postLimit=50&onlyImages=true

Where:

* myCuratorApiKey : your API key from the Curator.io applicaton
* postLimit [optional] : the number of posts you want to pull (default 100)
* onlyImages : allows you to specify if you only wish to have posts with an image (default: false)
